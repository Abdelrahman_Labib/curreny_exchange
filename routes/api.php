<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::namespace('Api')->group(function () {
    Route::post('register', 'Auth\RegisterUser');
    Route::post('register_social', 'Auth\SocialRegisterUser');
    Route::post('code_send', 'Auth\SendUserConfirmationCode');
    Route::post('code_check', 'Auth\VerifyUserConfirmationCode');
    Route::post('login', 'Auth\UserLogin');

    Route::middleware('auth:api')->group(function(){
        Route::post('logout', 'Auth\UserLogout');
        Route::post('password_change', 'Auth\ChangeUserPassword');
        Route::post('get_profile', 'Profile\GetProfileDetails');
        Route::post('set_profile', 'Profile\SetProfileDetails');
    });

    Route::get('countries', 'Countries\GetCountries');
    Route::get('companies', 'Companies\GetCompanies');
    Route::post('companies_exchange', 'Companies\GetCompaniesExchange');
    Route::post('branch', 'Companies\GetBranchDetails');
    Route::post('nearest_companies', 'Companies\GetNearestByCompany');
});

Route::namespace('Api\Datatable')->prefix('/datatable')->group(function (){
    Route::get('countries', 'GetCountries');
    Route::get('companies', 'GetCompanies');
    Route::get('branches', 'GetBranches');
});
