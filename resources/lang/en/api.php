<?php

return [
    'registeredMessage'         => 'Registered Successfully,please activate.',
    'wrongEmailOrPassword'      => 'Wrong email or password.',
    'wrongEmailMessage'         => 'Wrong email.',
    'emailExistsMessage'        => 'Email already exists.',
    'phoneExistsMessage'        => 'Phone already exists.',
    'accountIsSuspended'        => 'Code sent to your mail, activate the account .',
    'registeredSuccessMessage'  => 'Registered successfully.',
    'loggedInSuccessMessage'    => 'Logged in successfully.',
    'loggedOutSuccessMessage'   => 'Logged out successfully',
    'updateSuccessMessage'      => 'Updated successfully.',
    'successMessage'            => 'Saved successfully.',
    'getDataSuccessMessage'     => 'Data success.',
    'verifyMessage'             => 'Verified successfully.',
    'passResetMessage'          => 'Password has been reset successfully.',
    'codeSentMessage'           => 'Code sent successfully.',
];
