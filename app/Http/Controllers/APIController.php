<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class APIController extends Controller
{
    public function successResponse($message, $data = null)
    {
        return [
            'status'    => 'success',
            'message'   => __('api.' . $message),
            'data'      => $data
        ];
    }

    public function failedResponse($message)
    {
        return [
            'status'    => 'failed',
            'message'   => __('api.' . $message),
        ];
    }
}
