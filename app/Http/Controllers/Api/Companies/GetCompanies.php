<?php

namespace App\Http\Controllers\Api\Companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetCompanies extends Controller
{
    public function __invoke()
    {
        $companies = DB::table('companies')->select('id', 'name', 'logo')->get();
        return ['data' => $companies];
    }
}
