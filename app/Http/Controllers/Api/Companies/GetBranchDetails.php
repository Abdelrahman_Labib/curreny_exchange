<?php

namespace App\Http\Controllers\Api\Companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GetBranchDetails extends Controller
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'branch_id'  => 'required|exists:company_branches,id',
        ];

        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validate->messages()]);
        }

        $branch =
        DB::table('company_branches')
        ->select('name', 'address', 'phone', 'open_hours', 'latitude', 'longitude')
        ->find($request->branch_id);

        return ['status' => 'success', 'data' => $branch];
    }
}
