<?php

namespace App\Http\Controllers\Api\Companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GetNearestByCompany extends Controller
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'company_id'  => 'required|exists:companies,id',
            'latitude'    => 'required|numeric',
            'longitude'   => 'required|numeric',
        ];

        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validate->messages()]);
        }

        //GET NEAREST BRANCH BY COMPANY
        $haversine = $this->getRawHaversine($request->latitude, $request->longitude);
        $radius = 50;

        $branches =
        DB::table('company_branches')
        ->select('id', 'name', 'latitude', 'longitude')
        ->whereRaw("{$haversine} < ?", [$radius])
        ->where('company_id', $request->company_id)
        ->get();

        return ['data' => $branches];
    }

    public function getRawHaversine($lat1, $lng1)
    {
        return
            "(6371 * acos(cos(radians({$lat1}))
         * cos(radians(company_branches.latitude))
         * cos(radians(company_branches.longitude)
         - radians({$lng1}))
         + sin(radians({$lat1}))
         * sin(radians(company_branches.latitude))))";
    }
}
