<?php

namespace App\Http\Controllers\Api\Companies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GetCompaniesExchange extends Controller
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'country_id'  => 'required|exists:countries,id',
            'price'       => 'required|numeric'
        ];

        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['status' => 'failed', 'msg' => $validate->messages()]);
        }

        //CHECK COUNTRY EXISTENCE
        $country = DB::table('countries')->select('id', 'price', 'currency')->find($request->country_id);

        //GET LOCALE EXCHANGE
        $local = (int) $country->price * $request->price;
        $server = url('');

        //GET COMPANIES BY LOWER PRICE
        $companies =
        DB::table('companies')
        ->select([
            'id',
            'name',
            DB::raw('euro * ' . $request->price .' as euro'),
            DB::raw('dollar * ' . $request->price .' as dollar'),
            DB::raw("concat('$local') as local"),
            DB::raw("concat('$country->currency') as local_symbol"),
            DB::raw("CASE WHEN sc_type = 'percentage'  THEN concat(shipping_cost , '%')  ELSE shipping_cost END  as  type"),
            'description',
            'logo',
//            DB::raw("concat('$server/storage/', logo) as logo"),
        ])
        ->orderBy('dollar', 'desc')
        ->get();

        return ['data' => $companies];
    }
}
