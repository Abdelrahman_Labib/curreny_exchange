<?php

namespace App\Http\Controllers\Api\Countries;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetCountries extends Controller
{
    public function __invoke()
    {
        //GET ALL COUNTRIES
        $server = url('');
        $countries =
        DB::table('countries')
        ->select([
            'id',
            'name',
            'image',
//            DB::raw("concat('$server/storage/', image) as image"),
        ])
        ->get();

        return ['status' => 'success', 'data' => $countries];
    }
}
