<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\APIController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterUser extends APIController
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'name'      => 'required|max:190',
            'email'     => 'required|email|max:190',
            'password'  => 'required|max:190',
            'image'     => 'sometimes|image'
        ];

        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validate->messages()]);
        }

        //CHECK EMAIL EXISTENCE
        $email = DB::table('users')->where('email', $request->email)->select('id')->first();
        if ($email) {
            return $this->failedResponse('emailExistsMessage');
        }

        //CHECK PHONE EXISTENCE
        if($request->phone != null){
            $phone = DB::table('users')->where('phone', $request->phone)->select('id')->first();
            if ($phone) {
                return $this->failedResponse('phoneExistsMessage');
            }
        }

        //CREATE NEW USER
        $user = DB::table('users')->insertGetId([
            'api_token'  => Str::random(20),
            'name'       => $request->name,
            'email'      => $request->email,
            'phone'      => $request->phone,
            'password'   => Hash::make($request->password),
            'image'      => $request->image,
            'created_at' => now(),
        ]);

        //ACTIVATION CODE
        $activation_code = 1111;
//        $activation_code = rand(1000, 9999);

        //CREATE VERIFICATION CODE
        $this->verificationCode($user, 'verify', 'email', $request->email, $activation_code);

        //SEND MAIL ACTIVATION
//        $data = [
//            'name'      => $request->name,
//            'subject'   => 'Confirmation code currency exchange: ' . $activation_code,
//        ];
//
//        Mail::send('activation', $data, function ($message) use ($data,$request) {
//            $message->from('asd@asd.com','Activation@Currency')
//                    ->to($request->email)
//                    ->subject('Account activation');
//        });

        return $this->successResponse('registeredMessage');
    }

    public static function verificationCode($user_id, $objective, $information_type, $information, $activation_code)
    {
        DB::table('verification_codes')->updateOrInsert(
            [
                'user_id'           => $user_id,
                'objective'         => $objective,
                'information_type'  => $information_type,
                'information'       => $information,
            ],
            [
                'code'              => $activation_code,
                'expired_at'        => Carbon::now()->addHour()->toDateTimeString(),
                'created_at'        => now(),
            ]
        );
    }
}
