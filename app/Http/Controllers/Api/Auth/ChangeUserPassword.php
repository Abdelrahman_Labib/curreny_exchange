<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ChangeUserPassword extends APIController
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'password' => 'required',
        ];
        $validate = Validator::make($request->all(), $rules);

        if($validate->fails()){
            return ['status' => 'failed', 'message' => $validate->messages()];
        }

        //RESET USER PASSWORD
        $request->user()->update(['password' => Hash::make($request->password)]);

        return $this->successResponse('passResetMessage');
    }
}
