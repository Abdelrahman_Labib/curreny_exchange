<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\APIController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class VerifyUserConfirmationCode extends APIController
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'type'  => 'required|in:verify,reset_password',
            'email' => 'required',
            'code'  => 'required',
        ];
        $validate = Validator::make($request->all(), $rules);

        if($validate->fails()){
            return ['status' => 'failed', 'message' => $validate->messages()];
        }

        //CHECK EMAIL EXISTENCE
        $email = DB::table('users')->where('email', $request->email)->select('id')->first();
        if (!$email) {
            return $this->failedResponse('wrongEmailMessage');
        }

        $check =
        DB::table('verification_codes')
        ->where('information', $request->email)
        ->where('code', $request->code)
        ->where('objective', $request->type)
        ->where('expired_at', '>', now())
        ->first();

        if (!$check) {
            return ['status' => 'failed', 'message' => 'Invalid code.'];
        }

        //CHECK TYPE
        $check = $request->type == 'verify';

        //CHECK USER EXISTENCE
        $user = User::where('email', $request->email)->select('id', 'active', 'api_token as jwt')->first();
        if($check){
            $user->active = 1;
            $user->save();
        }

        //USER UNSET
        unset($user->id, $user->active, $user->updated_at);

        return $this->successResponse('verifyMessage', $user);

    }
}
