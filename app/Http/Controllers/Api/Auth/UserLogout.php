<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserLogout extends APIController
{
    public function __invoke(Request $request)
    {
        //UPDATE DEVICE TOKEN
        $request->user()->update([ 'device_token' => null ]);

        return $this->successResponse('loggedOutSuccessMessage');
    }
}
