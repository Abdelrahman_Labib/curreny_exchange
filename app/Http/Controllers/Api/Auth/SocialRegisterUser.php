<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\APIController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SocialRegisterUser extends APIController
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'name'          => 'required|max:190',
            'email'         => 'required|email|max:190',
            'device_token'  => 'required',
            'type'          => 'required|in:facebook,google',
            'image'         => 'sometimes|image',
        ];

        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validate->messages()]);
        }

        $user = User::where('email', $request->email)->select('social_media', 'api_token as jwt')->first();
        if ($user){
            //CHECK IF NOT SOCIAL MEDIA ACCOUNT
            if($user->social_media == 0){
                return $this->failedResponse('emailExistsMessage');
            }
            unset($user->social_media);

            return $this->successResponse('loggedInSuccessMessage', $user);
        }else{
            //CREATE NEW USER
            $user = User::select('api_token as jwt')->create([
                'api_token'               => Str::random(20),
                'active'                  => 1,
                'social_media'            => 1,
                'social_media_objective'  => $request->type,
                'name'                    => $request->name,
                'email'                   => $request->email,
                'device_token'            => $request->device_token,
                'image'                   => $request->image,
            ]);
            $user->jwt = $user->api_token;

            //USER UNSET ATTRIBUTES
            unset($user->id,$user->active,$user->social_media_objective,$user->api_token,$user->social_media,
                $user->name,$user->email,$user->image,$user->device_token,$user->created_at,$user->updated_at);

            return $this->successResponse('registeredSuccessMessage', $user);
        }
    }
}
