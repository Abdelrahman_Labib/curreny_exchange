<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SendUserConfirmationCode extends APIController
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'type'  => 'required|in:verify,reset_password',
            'email' => 'required',
        ];
        $validate = Validator::make($request->all(), $rules);

        if($validate->fails()){
            return ['status' => 'failed', 'message' => $validate->messages()];
        }

        //CHECK USER EXISTENCE
        $user =
        DB::table('users')
        ->where('email', $request->email)
        ->select('id', 'name')
        ->first();

        if(!$user){
            return $this->failedResponse('wrongEmailMessage');
        }

        //ACTIVATION CODE
        $activation_code = 1111;

        //CREATE VERIFICATION CODE
        RegisterUser::verificationCode($user->id, $request->type, 'email', $request->email, $activation_code);

        //SEND MAIL ACTIVATION
//        $data = [
//            'name'      => $user->name,
//            'subject'   => 'Confirmation code currency exchange: ' . $activation_code,
//        ];
//
//        Mail::send('activation', $data, function ($message) use ($data,$request) {
//            $message->from('asd@asd.com','Activation@Currency')
//                    ->to($request->email)
//                    ->subject('Account activation');
//        });

        return $this->successResponse('codeSentMessage');
    }
}
