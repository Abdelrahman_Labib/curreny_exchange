<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserLogin extends APIController
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'email'         => 'required|max:190',
            'password'      => 'required',
            'device_token'  => 'required|max:190'
        ];
        $validate = Validator::make($request->all(), $rules);

        if($validate->fails()){
            return ['status' => 'failed', 'message' => $validate->messages()];
        }

        //CHECK USER EXISTENCE
        $user =
        DB::table('users')
        ->where('email', $request->email)
        ->select('id', 'active', 'password', 'api_token as jwt')
        ->first();

        //CHECK USER CREDENTIALS
        if (!$user || !Hash::check($request->password, $user->password)) {
            return $this->failedResponse('wrongEmailOrPassword');
        }

        //CHECK USER ACTIVATION
        if ($user->active == 0) {
            $activation_code = 1111;

            //CREATE VERIFICATION CODE
            RegisterUser::verificationCode($user->id, 'verify', 'email', $request->email, $activation_code);
            return ['status' => 'failed_suspend', 'message' => __('api.accountIsSuspended')];
        }

        //USER UNSET
        unset($user->id, $user->active, $user->password);

        //INSERT OR UPDATE DEVICE TOKEN
        DB::table('users')->where('api_token', $user->jwt)->update([ 'device_token' => $request->device_token ]);

        return $this->successResponse('loggedInSuccessMessage', $user);
    }
}
