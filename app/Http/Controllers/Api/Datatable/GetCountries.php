<?php

namespace App\Http\Controllers\Api\Datatable;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class GetCountries extends Controller
{
    public function __invoke(Request $request)
    {
        $draw   = $request->get('draw');
        $start  = $request->start; //Start is the offset
        $length = $request->length; //How many records to show
        $search = $request->search['value'];

        //Sets the current page
        Paginator::currentPageResolver(function () use ($start, $length) {
            return ($start / $length + 1);
        });

        $server = url('');
        $countries =
        DB::table('countries')
        ->select([
            'id',
            'name',
            'currency',
            'price',
            DB::raw("concat('$server/storage/', countries.image) as image")
        ])
        ->where('id', 'like', '%'.$search.'%')
        ->orWhere('name', 'like', '%'.$search.'%')
        ->orWhere('currency', 'like', '%'.$search.'%')
        ->orWhere('price', 'like', '%'.$search.'%')
        ->orderBy('id', 'desc')
        ->paginate(10);

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $countries->total(),
            'recordsFiltered' => $countries->total(),
            'data' => $countries,
        );

        return $data;
    }
}
