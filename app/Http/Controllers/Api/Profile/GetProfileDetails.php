<?php

namespace App\Http\Controllers\Api\Profile;

use App\Http\Controllers\APIController;
use App\Http\Resources\ProfileResource;
use Illuminate\Http\Request;

class GetProfileDetails extends APIController
{
    public function __invoke(Request $request)
    {
        //PROFILE RESOURCE
        $user = new ProfileResource($request->user());

        return $this->successResponse('getDataSuccessMessage', $user);
    }
}
