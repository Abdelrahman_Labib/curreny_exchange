<?php

namespace App\Http\Controllers\Api\Profile;

use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SetProfileDetails extends APIController
{
    public function __invoke(Request $request)
    {
        //VALIDATE REQUEST
        $rules = [
            'name'      => 'required|max:190',
            'email'     => 'required|email|max:190',
            'image'     => 'sometimes|image'
        ];

        $validate = Validator::make($request->all(), $rules);
        if ($validate->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validate->messages()]);
        }

        //CHECK EMAIL EXISTENCE
        $email_check =
        DB::table('users')
        ->where('id', '!=', $request->user()->id)
        ->where('email', $request->email)->select('id')
        ->first();
        if ($email_check) {
            return $this->failedResponse('emailExistsMessage');
        }

        //CHECK PHONE EXISTENCE
        if($request->phone != null){
            $phone_check =
            DB::table('users')
            ->where('id', '!=', $request->user()->id)
            ->where('phone', $request->phone)
            ->select('id')
            ->first();
            if ($phone_check) {
                return $this->failedResponse('phoneExistsMessage');
            }
        }

        //CREATE NEW USER
        $request->user()->update([
            'name'       => $request->name,
            'email'      => $request->email,
            'phone'      => $request->phone,
        ]);

        return $this->successResponse('updateSuccessMessage');
    }
}
